% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/clustFuncBare3.r
\name{grabBatch}
\alias{grabBatch}
\title{DC: Grab Batch samples}
\usage{
grabBatch(XS, peakTab, QC, inj)
}
\arguments{
\item{XS}{An XCMS object}

\item{peakTab}{peaktable. Defaults to `xcms::peakTab(SX)`. Specify `PTalign` from `batchAlign` object for multibatch aligned data.}

\item{QC}{a grabQC object}

\item{inj}{a vector of batch injection numbers in sequence. Defaults to defined function to suit my naming strategy. Pls change accordingly.}
}
\value{
a list containing:

inj: a vector of sample injection numbers in sequence (same as indata OR generated)

Feats: Features (peak table)
}
\description{
Bring out all batch samples from an XCMS object: Exclude features missing from QC samples
}
